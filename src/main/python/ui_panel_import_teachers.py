# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'src/main/python/panel_import_teachers.ui',
# licensing of 'src/main/python/panel_import_teachers.ui' applies.
#
# Created: Thu Aug 27 17:43:48 2020
#      by: pyside2-uic  running on PySide2 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_PanelImportTeachers(object):
    def setupUi(self, PanelImportTeachers):
        PanelImportTeachers.setObjectName("PanelImportTeachers")
        PanelImportTeachers.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(PanelImportTeachers)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(PanelImportTeachers)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label.setFont(font)
        self.label.setMargin(5)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.actionFilterCombo = ActionsFilterComboBox(PanelImportTeachers)
        self.actionFilterCombo.setObjectName("actionFilterCombo")
        self.horizontalLayout_2.addWidget(self.actionFilterCombo)
        self.nameFilterEdit = QtWidgets.QLineEdit(PanelImportTeachers)
        self.nameFilterEdit.setClearButtonEnabled(True)
        self.nameFilterEdit.setObjectName("nameFilterEdit")
        self.horizontalLayout_2.addWidget(self.nameFilterEdit)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.tableView = ImportUsersTableView(PanelImportTeachers)
        self.tableView.setObjectName("tableView")
        self.verticalLayout.addWidget(self.tableView)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.syncButton = QtWidgets.QPushButton(PanelImportTeachers)
        self.syncButton.setObjectName("syncButton")
        self.horizontalLayout.addWidget(self.syncButton)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(PanelImportTeachers)
        QtCore.QMetaObject.connectSlotsByName(PanelImportTeachers)

    def retranslateUi(self, PanelImportTeachers):
        PanelImportTeachers.setWindowTitle(QtWidgets.QApplication.translate("PanelImportTeachers", "Form", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("PanelImportTeachers", "Enseignants", None, -1))
        self.syncButton.setText(QtWidgets.QApplication.translate("PanelImportTeachers", "Synchroniser", None, -1))

from tools.ui.import_users_table_view import ImportUsersTableView
from tools.ui.actions_filter_combobox import ActionsFilterComboBox
