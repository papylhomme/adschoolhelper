# This Python file uses the following encoding: utf-8

from PySide2.QtWidgets import QDialog, QMessageBox

from tools.ad import ADHolder


# Connection dialog
class ConnectDialog(QDialog):

    # Initialize the dialog
    def __init__(self):
        super(ConnectDialog, self).__init__()

        from ui_connect_dialog import Ui_ConnectDialog
        self.ui = Ui_ConnectDialog()
        self.ui.setupUi(self)

        self.ui.connectButton.clicked.connect(self.connect)


    # Handle connection
    def connect(self):
        try:
            ADHolder.init(self.ui.usernameEdit.text(), self.ui.passwordEdit.text())
            self.accept()
        except:
            QMessageBox.critical(self, "Erreur", "Erreur de connexion au serveur ActiveDirectory")

