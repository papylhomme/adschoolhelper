# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'tab_students.ui',
# licensing of 'tab_students.ui' applies.
#
# Created: Tue Nov 10 12:33:18 2020
#      by: pyside2-uic  running on PySide2 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_TabStudents(object):
    def setupUi(self, TabStudents):
        TabStudents.setObjectName("TabStudents")
        TabStudents.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(TabStudents)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(TabStudents)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.refreshButton = QtWidgets.QToolButton(TabStudents)
        self.refreshButton.setObjectName("refreshButton")
        self.horizontalLayout.addWidget(self.refreshButton)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.groupsFilterCombo = GroupsFilterComboBox(TabStudents)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupsFilterCombo.sizePolicy().hasHeightForWidth())
        self.groupsFilterCombo.setSizePolicy(sizePolicy)
        self.groupsFilterCombo.setObjectName("groupsFilterCombo")
        self.horizontalLayout_2.addWidget(self.groupsFilterCombo)
        self.filterEdit = QtWidgets.QLineEdit(TabStudents)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(4)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.filterEdit.sizePolicy().hasHeightForWidth())
        self.filterEdit.setSizePolicy(sizePolicy)
        self.filterEdit.setClearButtonEnabled(True)
        self.filterEdit.setObjectName("filterEdit")
        self.horizontalLayout_2.addWidget(self.filterEdit)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.tableView = UsersTableView(TabStudents)
        self.tableView.setObjectName("tableView")
        self.verticalLayout.addWidget(self.tableView)

        self.retranslateUi(TabStudents)
        QtCore.QMetaObject.connectSlotsByName(TabStudents)

    def retranslateUi(self, TabStudents):
        TabStudents.setWindowTitle(QtWidgets.QApplication.translate("TabStudents", "Form", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("TabStudents", "Elèves", None, -1))
        self.refreshButton.setText(QtWidgets.QApplication.translate("TabStudents", "...", None, -1))
        self.filterEdit.setPlaceholderText(QtWidgets.QApplication.translate("TabStudents", "Filtrer", None, -1))

from tools.ui.users_table_view import UsersTableView
from tools.ui.groups_filter_combobox import GroupsFilterComboBox
