# This Python file uses the following encoding: utf-8

import qtawesome as qta
from tools.ad import reset_user_password, delete_user, create_user, update_user, enable_user, disable_user

from .passwords_exporter import PasswordsExporter

# Base action
class Action():

    ROW_UPDATED = 1
    ROW_DELETED = 2

    STATUS_PENDING = 1
    STATUS_RUNNING = 2
    STATUS_DONE = 3
    STATUS_ERROR = 4

    COLOR_RUNNING = 'blue'
    COLOR_ERROR = 'red'
    COLOR_DONE = 'green'


    def __init__(self, action_icon, payload = None):
        (icon, color) = action_icon
        self.icon = icon
        self.color = color
        self.payload = payload
        self.status = Action.STATUS_PENDING
        self.message = None


    def set_error(self, error):
        self.status = Action.STATUS_ERROR
        self.message = error


    def get_icon(self):
        if self.status == Action.STATUS_PENDING:
            return qta.icon(self.icon, color = self.color)
        elif self.status == Action.STATUS_RUNNING:
            return qta.icon(self.icon, color = Action.COLOR_RUNNING)
        elif self.status == Action.STATUS_DONE:
            return qta.icon(self.icon, color = Action.COLOR_DONE)
        elif self.status == Action.STATUS_ERROR:
            return qta.icon(self.icon, color = Action.COLOR_ERROR)


# Action for resetting users passwords
class ActionResetPassword(Action):

    ICON = ('mdi.account-key', 'orange')

    def __init__(self, user):
        Action.__init__(self, ActionResetPassword.ICON, user)


    def execute(self):
        user = self.payload
        user.gen_password()
        reset_user_password(user)

        self.message = f"Nouveau mot de passe pour {user.username}: {user.password}"
        PasswordsExporter.add_user(user)

        print(f'New password for user {user.username}: {user.password}', flush = True)
        return Action.ROW_UPDATED


# Action for creating users
class ActionCreateUser(Action):

    ICON = ('mdi.account-plus', 'springgreen')

    def __init__(self, user):
        Action.__init__(self, ActionCreateUser.ICON, user)


    def execute(self):
        user = self.payload

        if user.password == "" or user.password == None:
            user.gen_password()

        create_user(user)

        self.message = f"Nouvel utilisateur {user.username}: {user.password}"
        PasswordsExporter.add_user(user)

        print(f'User {user.username} created with password {user.password}', flush = True)
        return Action.ROW_UPDATED


# Action for updating users
class ActionUpdateUser(Action):

    ICON = ('mdi.account-edit', 'gold')

    def __init__(self, user, new_ou = None):
        Action.__init__(self, ActionUpdateUser.ICON, user)
        self.new_ou = new_ou


    def execute(self):
        user = self.payload
        update_user(user, self.new_ou)
        self.message = f"Utilisateur {user.username} mis à jour"
        print(f'User {user.username} updated', flush = True)
        return Action.ROW_UPDATED


# Action for enabling users
class ActionEnableUser(Action):

    ICON = ('mdi.account-check', 'steelblue')

    def __init__(self, user):
        Action.__init__(self, ActionEnableUser.ICON, user)


    def execute(self):
        user = self.payload
        enable_user(user)
        self.message = f"Utilisateur {user.username} déverrouillé"
        print(f'User {user.username} enabled', flush = True)
        return Action.ROW_UPDATED


# Action for disabling users
class ActionDisableUser(Action):

    ICON = ('mdi.account-lock', 'magenta')

    def __init__(self, user):
        Action.__init__(self, ActionDisableUser.ICON, user)


    def execute(self):
        user = self.payload
        disable_user(user)
        self.message = f"Utilisateur {user.username} verrouillé"
        print(f'User {user.username} disabled', flush = True)
        return Action.ROW_UPDATED


# Action for deleting users
class ActionDeleteUser(Action):

    ICON = ('mdi.account-remove', 'purple')

    def __init__(self, user):
        Action.__init__(self, ActionDeleteUser.ICON, user)


    def execute(self):
        user = self.payload
        delete_user(user)
        self.message = f"Utilisateur {user.username} supprimé"
        print(f'User {user.username} deleted', flush = True)
        return Action.ROW_UPDATED
