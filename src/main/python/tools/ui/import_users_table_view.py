# This Python file uses the following encoding: utf-8

from .users_table_view import UsersTableView

# Custom widget for import users table view
class ImportUsersTableView(UsersTableView):

    # Initialize the widget
    def __init__(self, parent = None):
        super(ImportUsersTableView, self).__init__(parent)


    # Override on_context_menu to disable it
    def on_context_menu(self, point):
        return None


    # Override update_status_label to display a summary of actions
    def update_status_label(self):
        actions = self.table.model().get_summary()

        tmp = []
        if actions['error'] != 0:
            tmp.append(f"{actions['error']} erreurs")
        if actions['create'] != 0:
            tmp.append(f"{actions['create']} créations")
        if actions['delete'] != 0:
            tmp.append(f"{actions['delete']} suppressions")
        if actions['update'] != 0:
            tmp.append(f"{actions['update']} mises à jour")

        tmp.append(f"{self.table.model().rowCount()} éléments")

        self.label.setText(", ".join(tmp))
