# This Python file uses the following encoding: utf-8

from PySide2.QtCore import Qt
from PySide2.QtWidgets import QDialog, QStackedWidget, QHBoxLayout

from .panel_csv_import import PanelCSVImport


# Base dialog for CSV importation
class ImportDialog(QDialog):

    # Initialize the dialog
    def __init__(self, title, withClass, panel, file = None):
        QDialog.__init__(self)


        self.setWindowTitle(title)
        self.setWindowFlags(
            Qt.WindowTitleHint
            | Qt.WindowSystemMenuHint
            | Qt.WindowMinMaxButtonsHint
            | Qt.WindowCloseButtonHint
        )

        self.stackedWidget = QStackedWidget()
        layout = QHBoxLayout()
        layout.addWidget(self.stackedWidget)
        self.setLayout(layout)

        self.panel_csv = PanelCSVImport(withClass, file)
        self.panel_csv.importCSV.connect(self.process_csv)
        self.stackedWidget.addWidget(self.panel_csv)

        self.panel_table = panel
        self.stackedWidget.addWidget(self.panel_table)


    # Show the dialog (modal)
    def show(self):
        self.exec_()


    # Override close event to prevent closing when a process is running
    def closeEvent(self, event):
        if self.panel_table.is_operation_running():
            event.ignore()
        else:
            event.accept()


    # Override reject to prevent closing when a process is running
    def reject(self):
        if not self.panel_table.is_operation_running():
            super(ImportDialog, self).reject()


    # Set the CSV data to the panel and switch to it
    def process_csv(self):
        self.panel_table.set_csv_data(self.panel_csv.get_csv_data(), self.panel_csv.get_columns_indexes())
        self.stackedWidget.setCurrentWidget(self.panel_table)
