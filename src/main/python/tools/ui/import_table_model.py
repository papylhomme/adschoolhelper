# This Python file uses the following encoding: utf-8

from PySide2 import QtCore
from PySide2.QtCore import Qt, QModelIndex

from data.action import Action, ActionCreateUser, ActionDeleteUser, ActionUpdateUser
from data.users_table_model import UsersTableModel

from tools import ad as AD
from tools.worker import Worker

from .sort_table_model import SortTableModel
from .actions_filter_combobox import ActionsFilterComboBox as AFCB


class ImportTableModel(SortTableModel):

    # Initialize the model
    def __init__(self, OUPath, parent = None):
        super(ImportTableModel, self).__init__(UsersTableModel(), parent)

        self.OUPath = OUPath

        self.action_filter = 0

        self.threadpool = QtCore.QThreadPool()


    # Update the model with an action filter
    def update_action_filter(self, filter):
        self.beginResetModel()
        self.action_filter = filter
        self.endResetModel()


    # Get a summary of actions
    def get_summary(self):
        actions = {'create': 0, 'update': 0, 'delete': 0, 'error': 0}

        for user in self.get_local_users():
            action = self.get_action(user)

            if action and action.status == Action.STATUS_ERROR:
                actions['error'] = actions['error'] + 1

            if isinstance(action, ActionCreateUser):
                actions['create'] = actions['create'] + 1
            elif isinstance(action, ActionDeleteUser):
                actions['delete'] = actions['delete'] + 1
            elif isinstance(action, ActionUpdateUser):
                actions['update'] = actions['update'] + 1

        return actions


    # Implement row filtering
    def filterAcceptsRow(self, source_row, parent):
        # Early fallback when there is no action filtering
        if self.action_filter == 0:
            return super(ImportTableModel, self).filterAcceptsRow(source_row, parent)

        user = self.sourceModel().get_user(self.sourceModel().index(source_row, 0))

        if not self.filter_actions(user):
            return False

        return super(ImportTableModel, self).filterAcceptsRow(source_row, parent)


    # Set a list of users to import
    def set_import_users(self, users):
        self.import_users = users
        self.load_users()


    # Load users from AD
    def load_users(self):
        fetcher = Worker(self.merge_ad_users)
        fetcher.signals.result.connect(self.handle_merge_result)
        fetcher.signals.finished.connect(self.processing_finished)

        self.processing_started.emit(False)
        self.threadpool.start(fetcher)


    # Thread callback for loading and merging users from AD
    def merge_ad_users(self, progress_callback):
        AD.init_thread()

        pending_usernames = {}

        # Load existing users from AD
        ad_users = {}
        for user in AD.search_users(self.OUPath):
            ad_users[user.id] = user

        # Loop and create actions
        actions = {}
        merged_users = []
        for user in self.import_users:
            ad_user = ad_users.get(user.id, None)

            # User doesn't exists in AD, create it
            if ad_user == None:
                self.enforce_unique_username(user, pending_usernames)

                merged_users.append(user)
                actions[user.id] = ActionCreateUser(user)

                # Set an error if the id is missing
                if not user.id:
                    actions[user.id].set_error("ERROR: ID manquant")

            # User's information has changed, update it
            elif self.user_has_changed(ad_user, user):
                # Force the username and current OU to remain the same
                user.username = ad_user.username
                user.current_ou = ad_user.current_ou
                merged_users.append(user)

                if user.ad_path() != user.ad_current_path():
                    actions[user.id] = ActionUpdateUser(user, user.ad_dir())
                else:
                    actions[user.id] = ActionUpdateUser(user)

            # No change
            else:
                merged_users.append(user)

            ad_users.pop(user.id, None)

        # Delete remaining users (not present in import data)
        for user in ad_users.values():
            merged_users.append(user)
            actions[user.id] = ActionDeleteUser(user)

        return (merged_users, actions)


    # Handle ID collision
    def enforce_unique_username(self, user, pending_usernames):
        generated_username = AD.enforce_unique_username(user.username, pending_usernames)
        print(f'Gen username for {user.username}: {generated_username}', flush = True)

        user.username = generated_username
        pending_usernames[generated_username] = True


    # Check if a user needs update
    def user_has_changed(self, ad_user, user):
        if ad_user.firstname != user.firstname:
            return True

        if ad_user.lastname != user.lastname:
            return True

        if ad_user.group != user.group:
            return True

        if ad_user.current_ou != user.ad_dir():
            return True

        return False


    # Callback when merge thread is finished
    def handle_merge_result(self, result):
        (users, actions) = result

        # Apply the dataset to the model
        self.sourceModel().set_users(users, actions)
        self.processing_finished.emit()


    # Execute the actions for synchronization
    def execute(self):
        self.sourceModel().start_worker(self.get_local_users())


    # Filter a user based on action
    def filter_actions(self, user):
        # No filter
        if self.action_filter == AFCB.ALL:
            return True

        user_action = self.sourceModel().get_action(user)

        # Filter unchanged
        if self.action_filter == AFCB.UNCHANGED and user_action == None:
            return True

        # Filter every actions
        if self.action_filter == AFCB.ALL_ACTIONS and user_action != None:
            return True

        # Filter create actions
        if self.action_filter == AFCB.CREATE and isinstance(user_action, ActionCreateUser):
            return True

        # Filter delete actions
        if self.action_filter == AFCB.DELETE and isinstance(user_action, ActionDeleteUser):
            return True

        # Filter update actions
        if self.action_filter == AFCB.UPDATE and isinstance(user_action, ActionUpdateUser):
            return True

        # Filter actions in error
        if self.action_filter == AFCB.ERROR and user_action and user_action.status == Action.STATUS_ERROR:
            return True

        # Filter every actions
        if self.action_filter == AFCB.WAITING and user_action != None and user_action.status != Action.STATUS_DONE:
            return True

        return False
