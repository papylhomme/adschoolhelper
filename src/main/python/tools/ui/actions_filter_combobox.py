# This Python file uses the following encoding: utf-8

from PySide2.QtWidgets import QComboBox


# Custom combobox for filtering import actions
class ActionsFilterComboBox(QComboBox):

    ALL = 0
    UNCHANGED = 1
    ALL_ACTIONS = 2
    CREATE = 3
    DELETE = 4
    UPDATE = 5
    ERROR = 6
    WAITING = 7

    def __init__(self, parent = None):
        super(ActionsFilterComboBox, self).__init__(parent)

        self.addItems([
            'Tout',
            'Inchangé',
            'Toutes les actions',
            'Création',
            'Suppression',
            'Mise à jour',
            'En erreur',
            'En attente'
        ])
