# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_about.ui',
# licensing of 'dialog_about.ui' applies.
#
# Created: Tue Nov 10 10:49:38 2020
#      by: pyside2-uic  running on PySide2 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_DialogAbout(object):
    def setupUi(self, DialogAbout):
        DialogAbout.setObjectName("DialogAbout")
        DialogAbout.resize(326, 123)
        self.verticalLayout = QtWidgets.QVBoxLayout(DialogAbout)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_3 = QtWidgets.QLabel(DialogAbout)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setWeight(75)
        font.setBold(True)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.verticalLayout.addWidget(self.label_3)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(DialogAbout)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.labelVersion = QtWidgets.QLabel(DialogAbout)
        self.labelVersion.setObjectName("labelVersion")
        self.horizontalLayout.addWidget(self.labelVersion)
        self.labelGitHash = QtWidgets.QLabel(DialogAbout)
        self.labelGitHash.setObjectName("labelGitHash")
        self.horizontalLayout.addWidget(self.labelGitHash)
        self.formLayout.setLayout(0, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout)
        self.label_5 = QtWidgets.QLabel(DialogAbout)
        self.label_5.setObjectName("label_5")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_5)
        self.label_6 = QtWidgets.QLabel(DialogAbout)
        self.label_6.setObjectName("label_6")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.label_6)
        self.label_7 = QtWidgets.QLabel(DialogAbout)
        self.label_7.setObjectName("label_7")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_7)
        self.label_8 = QtWidgets.QLabel(DialogAbout)
        self.label_8.setWordWrap(True)
        self.label_8.setObjectName("label_8")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.label_8)
        self.verticalLayout.addLayout(self.formLayout)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)

        self.retranslateUi(DialogAbout)
        QtCore.QMetaObject.connectSlotsByName(DialogAbout)

    def retranslateUi(self, DialogAbout):
        DialogAbout.setWindowTitle(QtWidgets.QApplication.translate("DialogAbout", "A propos", None, -1))
        self.label_3.setText(QtWidgets.QApplication.translate("DialogAbout", "ADSchoolHelper", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("DialogAbout", "Version :", None, -1))
        self.labelVersion.setText(QtWidgets.QApplication.translate("DialogAbout", "1.0", None, -1))
        self.labelGitHash.setText(QtWidgets.QApplication.translate("DialogAbout", "9df0afafe769e7ce10731ee3f9c670cbe3632cfe", None, -1))
        self.label_5.setText(QtWidgets.QApplication.translate("DialogAbout", "Source :", None, -1))
        self.label_6.setText(QtWidgets.QApplication.translate("DialogAbout", "<html><head/><body><p><a href=\"https://gitlab.com/papylhomme/adschoolhelper\"><span style=\" text-decoration: underline; color:#0000ff;\">https://gitlab.com/papylhomme/adschoolhelper</span></a></p></body></html>", None, -1))
        self.label_7.setText(QtWidgets.QApplication.translate("DialogAbout", "License :", None, -1))
        self.label_8.setText(QtWidgets.QApplication.translate("DialogAbout", "<html><head/><body><p>L\'application est sous license <a href=\"https://gitlab.com/papylhomme/adschoolhelper/-/blob/master/LICENSE\"><span style=\" text-decoration: underline; color:#0000ff;\">GPLv3</span></a>, créée à l\'aide de Qt Opensource Edition (<a href=\"https://gitlab.com/papylhomme/adschoolhelper/-/blob/master/LICENSE_QT\"><span style=\" text-decoration: underline; color:#0000ff;\">license</span></a>)</p></body></html>", None, -1))

