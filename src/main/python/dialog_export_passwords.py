# This Python file uses the following encoding: utf-8

import codecs

from PySide2 import QtCore, QtWidgets
from PySide2.QtCore import Qt, QModelIndex
import qtawesome as qta

from data.passwords_exporter import PasswordsExporter


class TableModel(QtCore.QAbstractTableModel):
    def __init__(self, users):
        super(TableModel, self).__init__()
        self.users = users

    def rowCount(self, parent = QModelIndex()):
        return len(self.users)

    def columnCount(self, parent = QModelIndex()):
        return 5

    def get_user(self, index):
        if not index.isValid():
            return None
        if index.row() >= self.rowCount():
            return None
        return self.users[index.row()]

    def data(self, index, role):
        if not index.isValid():
            return None

        if index.row() >= self.rowCount():
            return None

        if role == Qt.DisplayRole:
            section = index.column()
            user = self.users[index.row()]

            if section == 0:
                return user.lastname
            elif section == 1:
                return user.firstname
            elif section == 2:
                return user.username
            elif section == 3:
                return user.group
            elif section == 4:
                return user.password
        else:
            return None


    def headerData(self, section, orientation, role = Qt.DisplayRole):
        if role != Qt.DisplayRole:
            return None

        if orientation == Qt.Horizontal:
            if section == 0:
                return "Nom"
            elif section == 1:
                return "Prénom"
            elif section == 2:
                return "Utilisateur"
            elif section == 3:
                return "Groupe"
            elif section == 4:
                return "Mot de passe"
        else:
            return None


# Sort proxy for table views
class SortTableModel(QtCore.QSortFilterProxyModel):

    # Initialize the proxy model
    def __init__(self, data_model, parent = None):
        super(SortTableModel, self).__init__(parent)
        self.filter = None
        self.setSourceModel(data_model)


    # Update the filter
    def update_filter(self, filter):
        self.beginResetModel()
        self.filter = filter
        self.endResetModel()


    # Implement row filtering
    def filterAcceptsRow(self, source_row, parent):
        # Early exit to avoid fetching the user for nothing
        if self.filter == None or self.filter == "":
            return True

        user = self.sourceModel().get_user(self.sourceModel().index(source_row, 0))

        # Match found in firstname
        if self.filter in user.firstname:
            return True

        # Match found in lastname
        if self.filter in user.lastname:
            return True

        # Match found in username
        if self.filter in user.username:
            return True

        return False



class DialogExportPasswords(QtWidgets.QDialog):

    # Initialize the dialog
    def __init__(self):
        super(DialogExportPasswords, self).__init__()
        self.users = PasswordsExporter.get_users()

        # Setup UI
        from ui_dialog_export_passwords import Ui_DialogExportPasswords
        self.ui = Ui_DialogExportPasswords()
        self.ui.setupUi(self)

        self.ui.saveButton.clicked.connect(self.save)

        # Init table
        self.menu = QtWidgets.QMenu(self)
        self.menu.addAction(qta.icon('mdi.account-key'), "Copier le mot de passe", self.on_copy_password)

        self.model = TableModel(self.users)
        self.sortModel = SortTableModel(self.model)
        self.ui.tableView.setModel(self.sortModel)
        self.ui.tableView.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.tableView.customContextMenuRequested.connect(self.on_context_menu)

        # Init filter tools
        self.filter = ""
        self.timer = QtCore.QTimer(self)
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.filter_name)

        self.ui.filterEdit.textEdited.connect(self.on_filter_update)


    # Handle right click for context menu
    def on_context_menu(self, point):
        self.menu.exec_(self.ui.tableView.mapToGlobal(point))


    # Copy password to clipboard
    def on_copy_password(self):
        selected = self.ui.tableView.selectionModel().currentIndex()
        if selected.isValid():
            print(f'copy password {self.users[selected.row()].password}')
            QtWidgets.QApplication.clipboard().setText(self.users[selected.row()].password)


    # Delay name filtering
    def on_filter_update(self, filter):
        self.filter = filter
        self.timer.start(500)


    # Update the model with a name filter
    def filter_name(self):
        self.sortModel.update_filter(self.filter)


    # Show the dialog
    def show(self):
        self.exec_()


    # Save the content to a file
    def save(self):
        (filename, filter) = QtWidgets.QFileDialog.getSaveFileName(self, "Enregistrer", None, "CSV (*.csv)")

        text = 'nom;prenom;utilisateur;classe;password'
        for u in self.users:
            text = f'{text}\n{u.lastname};{u.firstname};{u.username};{u.group};{u.password}'
        text = text.replace("\n", "\r\n")

        with codecs.open(filename, "w", "utf-8") as file:
            file.write(text)
            PasswordsExporter.set_saved()
            self.close()
