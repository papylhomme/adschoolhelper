# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'tab_teachers.ui',
# licensing of 'tab_teachers.ui' applies.
#
# Created: Tue Nov 10 12:33:28 2020
#      by: pyside2-uic  running on PySide2 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_TabTeachers(object):
    def setupUi(self, TabTeachers):
        TabTeachers.setObjectName("TabTeachers")
        TabTeachers.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(TabTeachers)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(TabTeachers)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.refreshButton = QtWidgets.QToolButton(TabTeachers)
        self.refreshButton.setObjectName("refreshButton")
        self.horizontalLayout.addWidget(self.refreshButton)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.filterEdit = QtWidgets.QLineEdit(TabTeachers)
        self.filterEdit.setClearButtonEnabled(True)
        self.filterEdit.setObjectName("filterEdit")
        self.verticalLayout.addWidget(self.filterEdit)
        self.tableView = UsersTableView(TabTeachers)
        self.tableView.setObjectName("tableView")
        self.verticalLayout.addWidget(self.tableView)

        self.retranslateUi(TabTeachers)
        QtCore.QMetaObject.connectSlotsByName(TabTeachers)

    def retranslateUi(self, TabTeachers):
        TabTeachers.setWindowTitle(QtWidgets.QApplication.translate("TabTeachers", "Form", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("TabTeachers", "Enseignants", None, -1))
        self.refreshButton.setText(QtWidgets.QApplication.translate("TabTeachers", "...", None, -1))
        self.filterEdit.setPlaceholderText(QtWidgets.QApplication.translate("TabTeachers", "Filtrer", None, -1))

from tools.ui.users_table_view import UsersTableView
