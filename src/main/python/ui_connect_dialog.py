# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'src/main/python/connect_dialog.ui',
# licensing of 'src/main/python/connect_dialog.ui' applies.
#
# Created: Mon Nov  9 10:24:25 2020
#      by: pyside2-uic  running on PySide2 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_ConnectDialog(object):
    def setupUi(self, ConnectDialog):
        ConnectDialog.setObjectName("ConnectDialog")
        ConnectDialog.resize(307, 157)
        self.verticalLayout = QtWidgets.QVBoxLayout(ConnectDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(ConnectDialog)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setWeight(75)
        font.setBold(True)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.usernameEdit = QtWidgets.QLineEdit(ConnectDialog)
        self.usernameEdit.setObjectName("usernameEdit")
        self.verticalLayout.addWidget(self.usernameEdit)
        self.passwordEdit = QtWidgets.QLineEdit(ConnectDialog)
        self.passwordEdit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.passwordEdit.setObjectName("passwordEdit")
        self.verticalLayout.addWidget(self.passwordEdit)
        self.connectButton = QtWidgets.QPushButton(ConnectDialog)
        self.connectButton.setObjectName("connectButton")
        self.verticalLayout.addWidget(self.connectButton)

        self.retranslateUi(ConnectDialog)
        QtCore.QMetaObject.connectSlotsByName(ConnectDialog)

    def retranslateUi(self, ConnectDialog):
        ConnectDialog.setWindowTitle(QtWidgets.QApplication.translate("ConnectDialog", "Connexion", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("ConnectDialog", "Connexion Active Directory", None, -1))
        self.usernameEdit.setPlaceholderText(QtWidgets.QApplication.translate("ConnectDialog", "Identifiant", None, -1))
        self.passwordEdit.setPlaceholderText(QtWidgets.QApplication.translate("ConnectDialog", "Mot de passe", None, -1))
        self.connectButton.setText(QtWidgets.QApplication.translate("ConnectDialog", "Se connecter", None, -1))

