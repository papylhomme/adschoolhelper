# This Python file uses the following encoding: utf-8

import os

from pyad import adcontainer # Required to prevent an import error with pywintypes

import qtawesome as qta
from PySide2 import QtCore, QtWidgets

from dialog_create_user import DialogCreateUser
from panel_import_teachers import PanelImportTeachers
from panel_import_students import PanelImportStudents

from tools.ui.query_table_model import QueryTableModel
from tools.ui.sort_table_model import SortTableModel
from tools.ui.import_dialog import ImportDialog
from dialog_export_passwords import DialogExportPasswords

from tools.worker import Worker
from data.user import User


# Home panel of the application
class TabHome(QtWidgets.QWidget):

    # Create the panel
    def __init__(self):
        super(TabHome, self).__init__()

        from ui_tab_home import Ui_TabHome
        self.ui = Ui_TabHome()
        self.ui.setupUi(self)

        self.ui.createUserButton.setIcon(qta.icon('mdi.account-plus'))
        self.ui.importTeachersButton.setIcon(qta.icon('mdi.import'))
        self.ui.importStudentsButton.setIcon(qta.icon('mdi.import'))
        self.ui.exportPasswordsButton.setIcon(qta.icon('mdi.file-key-outline'))

        # Init query tools
        self.filter = ""
        self.timer = QtCore.QTimer(self)
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.search)

        # Init models and table
        self.model = QueryTableModel()
        self.ui.tableView.setModel(SortTableModel(self.model))

        # Connect UI slots
        self.ui.createUserButton.clicked.connect(self.create_user)
        self.ui.importTeachersButton.clicked.connect(self.import_teachers)
        self.ui.importStudentsButton.clicked.connect(self.import_students)
        self.ui.exportPasswordsButton.clicked.connect(self.export_passwords)


        self.ui.quickFilterEdit.textEdited.connect(self.on_filter_update)
        self.ui.quickFilterEdit.returnPressed.connect(self.execute_search)

        self.model.processing_started.connect(lambda: self.ui.quickFilterEdit.setEnabled(False))
        self.model.processing_finished.connect(lambda: (
            self.ui.quickFilterEdit.setEnabled(True),
            self.ui.quickFilterEdit.setFocus(QtCore.Qt.OtherFocusReason)
        ))


    # Resolve a file to its full path or None if it doesn't exist
    def resolve_file(self, filename):
        file = os.path.join(os.getcwd(), filename)
        if os.path.isfile(file):
            return file
        else:
            return None


    # Open the create user dialog
    def create_user(self):
        dialog = DialogCreateUser()
        dialog.show()


    # Open the import teachers dialog
    def import_teachers(self):
        dialog = ImportDialog("Importer des professeurs", False, PanelImportTeachers(), self.resolve_file("teachers.csv"))
        dialog.show()


    # Open the import teachers dialog
    def import_students(self):
        dialog = ImportDialog("Importer des élèves", True, PanelImportStudents(), self.resolve_file("students.csv"))
        dialog.show()


    # Open the export passwords dialog
    def export_passwords(self):
        dialog = DialogExportPasswords()
        dialog.show()


    # Set the focus on the quick filter when the panel is shown
    def on_show(self):
        self.ui.quickFilterEdit.setFocus(QtCore.Qt.OtherFocusReason)


    # Execute the search immediately
    def execute_search(self):
        self.filter = self.ui.quickFilterEdit.text()
        self.search()


    # Handle filter update and delay query execution
    def on_filter_update(self, filter):
        self.filter = filter

        if filter == "":
            self.model.clear()
        else:
            self.timer.start(300)


    # Execute the search query
    def search(self):
        ad_filter = f"'*{self.filter}*'"
        ad_query = {
            'where_clause': f"objectClass = 'user' and (cn = {ad_filter} or sn = {ad_filter} or givenName = {ad_filter})",
            'path': User.ROOT_PATH
            }

        self.model.search(ad_query)
