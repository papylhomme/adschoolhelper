# This Python file uses the following encoding: utf-8

from data.user import User
from base_tab import BaseTab

from ui_tab_teachers import Ui_TabTeachers

class TabTeachers(BaseTab):

    # AD query to retrieve teachers
    query = {
        'where_clause': "objectClass = 'user'",
        'path': User.TEACHER_PATH
    }


    # Init panel
    def __init__(self):
        super(TabTeachers, self).__init__(TabTeachers.query, Ui_TabTeachers())
