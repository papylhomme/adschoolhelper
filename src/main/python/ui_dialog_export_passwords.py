# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_export_passwords.ui',
# licensing of 'dialog_export_passwords.ui' applies.
#
# Created: Tue Nov 10 12:33:40 2020
#      by: pyside2-uic  running on PySide2 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_DialogExportPasswords(object):
    def setupUi(self, DialogExportPasswords):
        DialogExportPasswords.setObjectName("DialogExportPasswords")
        DialogExportPasswords.resize(400, 300)
        DialogExportPasswords.setModal(True)
        self.verticalLayout = QtWidgets.QVBoxLayout(DialogExportPasswords)
        self.verticalLayout.setObjectName("verticalLayout")
        self.filterEdit = QtWidgets.QLineEdit(DialogExportPasswords)
        self.filterEdit.setClearButtonEnabled(True)
        self.filterEdit.setObjectName("filterEdit")
        self.verticalLayout.addWidget(self.filterEdit)
        self.tableView = QtWidgets.QTableView(DialogExportPasswords)
        self.tableView.setAlternatingRowColors(True)
        self.tableView.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.tableView.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tableView.setObjectName("tableView")
        self.verticalLayout.addWidget(self.tableView)
        self.saveButton = QtWidgets.QPushButton(DialogExportPasswords)
        self.saveButton.setObjectName("saveButton")
        self.verticalLayout.addWidget(self.saveButton)

        self.retranslateUi(DialogExportPasswords)
        QtCore.QMetaObject.connectSlotsByName(DialogExportPasswords)

    def retranslateUi(self, DialogExportPasswords):
        DialogExportPasswords.setWindowTitle(QtWidgets.QApplication.translate("DialogExportPasswords", "Exportation des mots de passe", None, -1))
        self.filterEdit.setPlaceholderText(QtWidgets.QApplication.translate("DialogExportPasswords", "Filtrer", None, -1))
        self.saveButton.setText(QtWidgets.QApplication.translate("DialogExportPasswords", "Enregistrer dans un fichier", None, -1))

