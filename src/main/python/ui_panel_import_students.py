# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'src/main/python/panel_import_students.ui',
# licensing of 'src/main/python/panel_import_students.ui' applies.
#
# Created: Thu Aug 27 17:45:55 2020
#      by: pyside2-uic  running on PySide2 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_PanelImportStudents(object):
    def setupUi(self, PanelImportStudents):
        PanelImportStudents.setObjectName("PanelImportStudents")
        PanelImportStudents.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(PanelImportStudents)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(PanelImportStudents)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label.setFont(font)
        self.label.setMargin(5)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.actionFilterCombo = ActionsFilterComboBox(PanelImportStudents)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.actionFilterCombo.sizePolicy().hasHeightForWidth())
        self.actionFilterCombo.setSizePolicy(sizePolicy)
        self.actionFilterCombo.setObjectName("actionFilterCombo")
        self.horizontalLayout_2.addWidget(self.actionFilterCombo)
        self.groupFilterCombo = GroupsFilterComboBox(PanelImportStudents)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupFilterCombo.sizePolicy().hasHeightForWidth())
        self.groupFilterCombo.setSizePolicy(sizePolicy)
        self.groupFilterCombo.setObjectName("groupFilterCombo")
        self.horizontalLayout_2.addWidget(self.groupFilterCombo)
        self.nameFilterEdit = QtWidgets.QLineEdit(PanelImportStudents)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.nameFilterEdit.sizePolicy().hasHeightForWidth())
        self.nameFilterEdit.setSizePolicy(sizePolicy)
        self.nameFilterEdit.setClearButtonEnabled(True)
        self.nameFilterEdit.setObjectName("nameFilterEdit")
        self.horizontalLayout_2.addWidget(self.nameFilterEdit)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.tableView = ImportUsersTableView(PanelImportStudents)
        self.tableView.setObjectName("tableView")
        self.verticalLayout.addWidget(self.tableView)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.syncButton = QtWidgets.QPushButton(PanelImportStudents)
        self.syncButton.setObjectName("syncButton")
        self.horizontalLayout.addWidget(self.syncButton)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(PanelImportStudents)
        QtCore.QMetaObject.connectSlotsByName(PanelImportStudents)

    def retranslateUi(self, PanelImportStudents):
        PanelImportStudents.setWindowTitle(QtWidgets.QApplication.translate("PanelImportStudents", "Form", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("PanelImportStudents", "Elèves", None, -1))
        self.syncButton.setText(QtWidgets.QApplication.translate("PanelImportStudents", "Synchroniser", None, -1))

from tools.ui.actions_filter_combobox import ActionsFilterComboBox
from tools.ui.groups_filter_combobox import GroupsFilterComboBox
from tools.ui.import_users_table_view import ImportUsersTableView
