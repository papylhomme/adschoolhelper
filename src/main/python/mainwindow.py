# This Python file uses the following encoding: utf-8

import qtawesome as qta
from PySide2.QtWidgets import QMainWindow

from tab_ad_tree import TabADTree
from tab_home import TabHome
from tab_teachers import TabTeachers
from tab_students import TabStudents

from dialog_legend import DialogLegend
from dialog_about import DialogAbout


# MainWindow
class MainWindow(QMainWindow):

    # Setup the main window
    def __init__(self, parent = None):
        super(MainWindow, self).__init__(parent)

        from ui_mainwindow import Ui_MainWindow
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # Init menu
        self.ui.actionLegend.triggered.connect(lambda: DialogLegend().exec_())
        self.ui.actionAbout.triggered.connect(lambda: DialogAbout().exec_())

        # Init tabs
        self.tab_ad_tree = TabADTree()
        self.tab_home = TabHome()
        self.tab_teachers = TabTeachers()
        self.tab_students = TabStudents()

        self.ui.stackedWidget.addWidget(self.tab_ad_tree)
        self.ui.stackedWidget.addWidget(self.tab_home)
        self.ui.stackedWidget.addWidget(self.tab_teachers)
        self.ui.stackedWidget.addWidget(self.tab_students)

        self.ui.homeTabButton.clicked.connect(lambda: self.set_current_tab(self.tab_home))
        self.ui.homeTabButton.setIcon(qta.icon('mdi.home'))

        self.ui.teachersTabButton.clicked.connect(lambda: self.set_current_tab(self.tab_teachers))
        self.ui.teachersTabButton.setIcon(qta.icon('mdi.account-group'))

        self.ui.studentsTabButton.clicked.connect(lambda: self.set_current_tab(self.tab_students))
        self.ui.studentsTabButton.setIcon(qta.icon('mdi.account-group'))

        self.tab_ad_tree.tree_updated.connect(self.check_ad_tree)

        # Check the AD tree and set the AD Tree panel if initialization is needed
        if self.check_ad_tree():
            self.set_current_tab(self.tab_home)
        else:
            self.set_current_tab(self.tab_ad_tree)

        self.show()


    # Check the AD tree and disable the panels if it is missing an OU
    def check_ad_tree(self):
        tree_ok = self.tab_ad_tree.check_ad_tree()
        self.ui.homeTabButton.setEnabled(tree_ok)
        self.ui.teachersTabButton.setEnabled(tree_ok)
        self.ui.studentsTabButton.setEnabled(tree_ok)
        return tree_ok


    # Set the displayed tab
    def set_current_tab(self, tab):
        self.ui.stackedWidget.setCurrentWidget(tab)
        tab.on_show()
