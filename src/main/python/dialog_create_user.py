# This Python file uses the following encoding: utf-8

import sys
import traceback

import qtawesome as qta
from PySide2 import QtCore, QtWidgets

from tools import ad as AD
from data.teacher import Teacher
from data.student import Student
from data.action import ActionCreateUser


class DialogCreateUser(QtWidgets.QDialog):

    # Initialize the dialog
    def __init__(self):
        super(DialogCreateUser, self).__init__()

        # Setup UI and connect slots
        from ui_dialog_create_user import Ui_DialogCreateUser
        self.ui = Ui_DialogCreateUser()
        self.ui.setupUi(self)

        self.ui.buttonResetPassword.setIcon(qta.icon('mdi.refresh'))
        self.ui.buttonResetPassword.clicked.connect(self.gen_password)

        self.ui.radioButtonTeacher.clicked.connect(self.update_user_type)
        self.ui.radioButtonStudent.clicked.connect(self.update_user_type)

        self.ui.buttonCreate.clicked.connect(self.create_user)

        self.ui.lineEditID.textChanged.connect(self.update_user)
        self.ui.lineEditFirstName.textChanged.connect(self.update_user)
        self.ui.lineEditLastName.textChanged.connect(self.update_user)
        self.ui.lineEditClass.textChanged.connect(self.update_user)

        # Initialize the user
        self.update_user_type()


    # Show the dialog
    def show(self):
        self.exec_()


    # Update the user when type is modified
    def update_user_type(self):
        if self.ui.radioButtonTeacher.isChecked():
            self.user = Teacher(self.ui.lineEditID.text(), self.ui.lineEditFirstName.text(), self.ui.lineEditLastName.text(), self.ui.lineEditLogin.text())
            self.ui.labelClass.setVisible(False)
            self.ui.lineEditClass.setVisible(False)
        else:
            self.user = Student(self.ui.lineEditID.text(), self.ui.lineEditFirstName.text(), self.ui.lineEditLastName.text(), self.ui.lineEditClass.text(), self.ui.lineEditLogin.text())
            self.ui.labelClass.setVisible(True)
            self.ui.lineEditClass.setVisible(True)

        self.gen_password()


    # Update the user's data
    def update_user(self):
        self.user.id = self.ui.lineEditID.text().strip()
        self.user.firstname = self.ui.lineEditFirstName.text().strip()
        self.user.lastname = self.ui.lineEditLastName.text().strip()

        if isinstance(self.user, Student):
            self.user.group = self.ui.lineEditClass.text().strip()

        # Generate a new username
        self.user.gen_username()
        self.user.username = AD.enforce_unique_username(self.user.username, {})

        self.ui.lineEditLogin.setText(self.user.username)


    # Generate a new password
    def gen_password(self):
        self.user.gen_password()
        self.ui.lineEditPassword.setText(self.user.password)


    # Override accept to call create_user
    def accept(self):
        self.create_user()


    # Create the user and close the dialog
    def create_user(self):
        # Safety checks
        if self.user.id == "" or self.user.id == None:
            QtWidgets.QMessageBox.critical(self, "Erreur création d'utilisateur", "L'identifiant est incorrect")
            return
        if self.user.firstname == "" or self.user.firstname == None:
            QtWidgets.QMessageBox.critical(self, "Erreur création d'utilisateur", "Le prénom est incorrect")
            return
        if self.user.lastname == "" or self.user.lastname == None:
            QtWidgets.QMessageBox.critical(self, "Erreur création d'utilisateur", "Le nom est incorrect")
            return
        if isinstance(self.user, Student) and (self.user.group == "" or self.user.group == None):
            QtWidgets.QMessageBox.critical(self, "Erreur création d'utilisateur", "La classe est incorrecte")
            return


        # Create the user in AD
        try:
            a = ActionCreateUser(self.user)
            a.execute()
            self.done(True)
        except:
            exctype, value = sys.exc_info()[:2]
            traceback.print_exc()
            QtWidgets.QMessageBox.critical(self, "Erreur création d'utilisateur", f'ERROR: {traceback.format_exception_only(exctype, value)[0].strip()}')

