# This Python file uses the following encoding: utf-8

from PySide2 import QtCore, QtWidgets


class DialogAbout(QtWidgets.QDialog):

    # Initialize the dialog
    def __init__(self):
        super(DialogAbout, self).__init__()

        # Setup UI and connect slots
        from ui_dialog_about import Ui_DialogAbout
        self.ui = Ui_DialogAbout()
        self.ui.setupUi(self)
